package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {
	
	Vector3d vect1 = new Vector3d(4,5,6);
	Vector3d vect2 = new Vector3d(5,6,7);
	
	@Test
	void testGet() {
		assertEquals(4,vect1.getX());
		assertEquals(5,vect1.getY());
		assertEquals(6,vect1.getZ());
	}
	
	@Test
	void testMagnitude() {
		assertEquals(8.77496438739,vect1.magnitude(), 0.0001);
		assertEquals(10.4880884817,vect2.magnitude(), 0.0001);
	}
	
	@Test
	void testDotProduct() {
		assertEquals(92,vect1.dotProduct(vect2));
	}
	
	@Test
	void testAdd() {
		Vector3d vectTest = new Vector3d(9,11,13);
		Vector3d vectTest2 = vect1.add(vect2);
		
		assertEquals(vectTest.getX(),vectTest2.getX());
		assertEquals(vectTest.getY(),vectTest2.getY());
		assertEquals(vectTest.getZ(),vectTest2.getZ());
	}

}
