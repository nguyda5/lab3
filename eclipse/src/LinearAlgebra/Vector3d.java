package LinearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	public Vector3d(double userX, double userY, double userZ) {
		x = userX;
		y = userY;
		z = userZ;
	}
	
	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}
	
	public double getZ() {
		return this.z;
	}
	
	double magnitude() {
		double magnitudeOfVector = Math.sqrt(Math.pow(x, 2)+Math.pow(y, 2)+Math.pow(z, 2));
		return magnitudeOfVector;
	}
	
	double dotProduct(Vector3d secondVector) {
		
		double productValueX = this.x*secondVector.x;
		double productValueY = this.y*secondVector.y;
		double productValueZ = this.z*secondVector.z;
		
		double totalProduct = productValueX + productValueY + productValueZ;
		
		return totalProduct;
	}
	
	Vector3d add(Vector3d secondVector) {
		double newX = this.x + secondVector.x;
		double newY = this.y + secondVector.y;
		double newZ = this.z + secondVector.z;
		
		Vector3d newVector = new Vector3d(newX,newY,newZ);
		
		return newVector;
		
	}
	
}
